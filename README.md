# taskman

Task manager for Go.

Allows to execute tasks concurrently with a given concurrency limit. 
Every task can spawn an arbitrary amount of extra tasks.

```go
tm := &Taskman{10} // concurrency limit 10
// t1, t2, t3 must implement Task with one method: Execute() []Task
// Any tasks returned by tX.Execute() will also be executed by the taskman
tm.Run([]Task{t1, t2, t3})
```

Can be useful for web crawlers which recursively add more and more pages to crawl, while analyzing a page.
Inspired by this article: https://jorin.me/use-go-channels-to-build-a-crawler/