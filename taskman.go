package taskman

import (
	"sync"
)

type (
	Taskman struct {
		concurrency int
	}
	Task interface {
		Execute() []Task
	}
)

func (t *Taskman) Run(tasks []Task) {
	queueCount := 0
	wait := make(chan int)
	inWork := make(chan Task)
	queue := make(chan Task)

	// once queueCount reaches zero, we're done
	go func() {
		for delta := range wait {
			queueCount += delta
			if queueCount == 0 {
				close(queue)
			}
		}
	}()

	go func() {
		for task := range queue { // runs until queue channel is not closed by the code above
			inWork <- task
		}
		close(inWork)
		close(wait)
	}()

	// init workers
	var wg sync.WaitGroup
	for i := 0; i < t.concurrency; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			t.worker(inWork, queue, wait)
		}()
	}

	wait <- len(tasks)
	t.enqueue(queue, tasks)

	wg.Wait()
}

func (t *Taskman) worker(inWork chan Task, queue chan Task, wait chan int) {
	for task := range inWork {
		more := task.Execute()
		wait <- len(more) - 1
		go t.enqueue(queue, more)
	}
}

func (t *Taskman) enqueue(queue chan Task, tasks []Task) {
	for _, task := range tasks {
		queue <- task
	}
}
