package taskman

import (
	"reflect"
	"sort"
	"sync"
	"testing"
)

type (
	breeder struct {
	}
	final struct {
		name string
	}
)

var mu sync.Mutex
var log []string

func (b *breeder) Execute() []Task {
	writeLog("b")
	return []Task{
		&final{"f1"},
		&final{"f2"},
	}
}

func (f *final) Execute() []Task {
	writeLog(f.name)
	return nil
}

func writeLog(m string) {
	mu.Lock()
	log = append(log, m)
	mu.Unlock()
}

func TestTaskman_Run(t1 *testing.T) {
	b := &breeder{}
	tm := &Taskman{10}
	tm.Run([]Task{b, b, b, b, b})
	// 5 breeders write "b" 5 times, each produces 5 finals "f1" and 5 finals "f2"
	expected := []string{"b", "b", "b", "b", "b", "f1", "f1", "f1", "f1", "f1", "f2", "f2", "f2", "f2", "f2"}
	sort.Strings(log) // bc of concurrency, it might happen in a different order
	if !reflect.DeepEqual(log, expected) {
		t1.Errorf("Run(): want %v, got %v", expected, log)
	}
}
